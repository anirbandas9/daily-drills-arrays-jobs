const users = {
  John: {
    age: 24,
    desgination: "Senior Golang Developer",
    interests: ["Chess, Reading Comics, Playing Video Games"],
    qualification: "Masters",
    nationality: "Greenland",
  },
  Ron: {
    age: 19,
    desgination: "Intern - Golang",
    interests: ["Video Games"],
    qualification: "Bachelor",
    nationality: "UK",
  },
  Wanda: {
    age: 24,
    desgination: "Intern - Javascript",
    interests: ["Piano"],
    qualification: "Bachaelor",
    nationality: "Germany",
  },
  Rob: {
    age: 34,
    desgination: "Senior Javascript Developer",
    interest: ["Walking his dog, Cooking"],
    qualification: "Masters",
    nationality: "USA",
  },
  Pike: {
    age: 23,
    desgination: "Python Developer",
    interests: ["Listing Songs, Watching Movies"],
    qualification: "Bachaelor's Degree",
    nationality: "Germany",
  },
};

/*

Q1 Find all users who are interested in playing video games.
Q2 Find all users staying in Germany.
Q3 Sort users based on their seniority level 
   for Designation - Senior Developer > Developer > Intern
   for Age - 20 > 10
Q4 Find all users with masters Degree.
Q5 Group users based on their Programming language mentioned in their designation.

NOTE: Do not change the name of this file

*/
// Problem - 1
const interestInVideoGames = Object.keys(users).filter((user) => {
  if (users[user].interests !== undefined) {
    return users[user].interests.some((interest) =>
      interest.toLowerCase().includes("video games")
    );
  }
});

console.log(interestInVideoGames);

// Problem - 2
const usersInGermany = Object.keys(users).filter((user) => {
  return users[user].nationality === "Germany";
});

console.log(usersInGermany);

// Problem - 3
const sortedUsers = Object.keys(users).sort((userA, userB) => {
  const orderOfDesgn = { Senior: 1, Developer: 2, Intern: 3 };
  const userADesg = orderOfDesgn[users[userA].desgination.split(" ")[0]];
  const userBDesg = orderOfDesgn[users[userB].desgination.split(" ")[0]];

  if (userADesg === userBDesg) {
    return users[userA].age > users[userB].age ? -1 : 1;
  }
  return userADesg - userBDesg;
});

console.log(sortedUsers);

// Problem - 4
const userWithMasters = Object.keys(users).filter((user) => {
  return users[user].qualification.toLowerCase().includes("masters");
});

console.log(userWithMasters);

// Problem - 5
function groupUsersByLanguage() {
  const groupByLang = {};

  Object.keys(users).map((user) => {
    let language = "";
    const desgSplit = users[user].desgination.split(" ");
    const developer = desgSplit.indexOf("Developer");
    const hypen = desgSplit.indexOf("-");

    // Checking if developer present in designation then language index is before
    if (developer !== -1) {
      language = desgSplit[developer - 1];
    }
    // Checking if - present in designation then language index is after
    if (hypen !== -1) {
      language = desgSplit[hypen + 1];
    }
    groupByLang[language] = groupByLang[language] || [];
    groupByLang[language].push(user);
  });
  return groupByLang;
}

console.log(groupUsersByLanguage());
